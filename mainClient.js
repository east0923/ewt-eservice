'use strict';

module.exports=function (conf) {
    const mainClient=new c_mainClient();
    if(conf) mainClient.setConf(conf);
    return mainClient;
};

const econn=require('ewt-econn');
const etools=require('ewt-etools');

class c_mainClient extends etools.classBase{
    constructor(){
        // 默认配置
        super({
            mainHost:'localhost',
            mainPort:null,
            debug:false,
            serviceName:null
        });
        // 连接实例
        this._online=false;
        this._econnSocket=new econn.tcpSocket();
        {
            // tcp连接的loged事件即为对外的online事件
            this._econnSocket.on('loged',()=>{
                // 登录成后，向服务器报告自己的服务名
                this._econnSocket.send_jr({cmd:'service_client_login',serviceName:this._conf.serviceName});

                // 触发消息
                this._emitMsg('tcpSocket on');

                // 触发外部online事件
                if(!this._hasFirstLogin){
                    this.emit('firstOnline');
                    this._hasFirstLogin=true
                }
                // 之前不在线，触发online事件
                if(!this._online){
                    this.emit('online');
                    this._online=true;
                }
            });

            // tcp连接的close事件即为对外的offline事件
            this._econnSocket.on('close',()=>{
                // 触发消息
                this._emitMsg('tcpSocket off');

                // 之前在线，触发offline事件
                if(this._online){
                    this.emit('offline');
                    this._online=false;
                }
            });

            // 连接错误消息，报告为本服务的警告
            this._econnSocket.on('error',(code,err)=>{
                this._emitWarn(code,err);
            });
            // jr消息分发
            this._econnSocket.on('rec_jr',(json,raw)=>{
                if(typeof this['_jr_'+json.cmd]==='function') this['_jr_'+json.cmd](json,raw);
                else this._emitWarn(0,'undo JR msg: '+JSON.stringify(json))
            })
        }

        // 控制端传送来的配置信息
        this._serviceConf=null;

        // 内部API，(已初始化外部调用)
        this._innerAPI={
            _publicAPI:(apiReqJson,raw,callback)=>{
                // 当前服务是否支持该对外API
                if(!(apiReqJson.api in this._publicAPI)){
                    callback('no publicAPI in service: '+this.serviceName);
                    return
                }

                // 调用已注册的外部API方法
                this._publicAPI[apiReqJson.api](apiReqJson,raw,callback)
            }
        };

        // 对外API
        this._publicAPI={};

        // 内部服务间对外调用
        this.__innerReqNum=0;       // 序数计数，使用时通过_innerReqNum属性
        this._innerReqCallback={}   // 内部调用回调函数缓存
    }

    /* ============ classBase标准接口 =========== */
    // 启动服务
    _boot(callback){
        this._hasFirstLogin=false;// tcp连接的loged事件即为对外的online事件

        // 启动流程
        this._econnSocket.setConf({
            port:this._conf.mainPort,
            host:this._conf.mainHost,
            debug:this._conf.debug
        });
        this._econnSocket.start();

        // 启动脚本执行完毕，执行回调
        process.nextTick(()=>{
            callback();
        });
    }
    // 停止服务
    _stop(callback){
        if(this._econnSocket){
            this._econnSocket.stop();
        }
        callback();
    }
    // 销毁服务
    _destroy(){
        this._econnSocket.destroy();
    }

    /* ========== jr消息处理 ==========*/
    // 收到服务器端配置信息 (登录时，服务器端有改变时 被调用)
    _jr_serviceConf(json,raw){
        // 报告消息
        this._emitMsg('renew serviceConf: '+JSON.stringify(json.conf));
        // 根据是否已有配置项，确定触发的事件名
        const eventName=this._serviceConf?'confChange':'confInit';
        this._serviceConf=json.conf;
        this.emit(eventName);
    }
    // 收到innerAPI调用结果反馈
    _jr_innerAPI_Bak(resJson,raw){
        if(this._conf.debug) etools.log('[DEBUG] rec_jr innerAPI_Bak reqNum:'+resJson.reqNum);
        // 获取回调函数
        const callback=this._innerReqCallback[resJson.reqNum];
        // 如果有，则执行，并从缓存中删除
        if(typeof callback==="function"){
            callback(resJson.err,resJson.json,raw);
            delete this._innerReqCallback[resJson.reqNum];
        }
        // 没有，写日志
        else {
            this._emitWarn(0,'rec innerAPI_bak but has NO callback cache, maybe timeouted: '+JSON.stringify(resJson));
        }
    }
    // 收到中转API处理请求
    _jr_tranInnerAPI(reqJson,raw){
        // 触发msg
        this._emitMsg('rec tranInnerAPI: '+JSON.stringify(reqJson));
        // 可以处理中转到达的API请求
        if(reqJson.apiName in this._innerAPI){
            this._innerAPI[reqJson.apiName](reqJson.json,raw,(err,json,raw)=>{
                this._econnSocket.send_jr({
                    cmd:'tranInnerAPI_Bak',
                    reqNum:reqJson.reqNum,
                    serviceId:reqJson.serviceId,
                    err:err,
                    json:json
                },raw,(err)=>{
                    if(err) this._emitWarn(0,'[tranInnerAPI] '+err);
                })
            })
        }
        // 无法处理该API请求
        else {
            this._econnSocket.send_jr({
                cmd:'tranInnerAPI_Bak',
                reqNum:reqJson.reqNum,
                serviceId:reqJson.serviceId,
                err:`no API:${reqJson.apiName} in service:${this.serviceName}`,
            },raw,(err)=>{
                if(err) this._emitWarn(0,'[tranInnerAPI] '+err);
            })
        }
    }

    /* 属性 */
    get _innerReqNum(){
        // 归零检验
        if(this.__innerReqNum>2e9){ this.__innerReqNum=0}
        // 反馈累加
        return this.__innerReqNum++;
    }       // 内部服务间调用序数生成
    get serviceName(){
        return this._conf.serviceName
    }        // 服务名
    get isOnline(){
        // 与服务器连接是否在线
        return this._online
    }

    /* ========== 自身维护对外方法 ==========*/

    // 记录配置(此配置为进程中的配置，与服务端的serviceConf不同)
    setConf(conf){
        switch (typeof conf){
            case 'number':
            case 'string':
                this._renewConf({mainPort:Number.parseInt(conf)});
                break;
            case 'object':
                this._renewConf(conf);
                break;
        }
    }

    /* ========== 业务 对外方法 ==========*/
    // 发布内部API
    regInnerAPI(apiName,func){
        // 输入检验，出错则为编程错误
        if(typeof apiName!=='string'||!apiName) throw new Error('apiName must be STRING');
        if(typeof func!=='function') throw new Error('func must be FUNCTION');

        // 内部记录
        this._innerAPI[apiName]=func;
    }
    // 调用内部API
    innerAPI(...other){
        // 参数顺序整理
        const [apiFullName,json,raw,timeout,callback]=
            etools.funcParamsOrder(other,['string',['object','array'],'buffer','float','function']);

        // 没有api名称或回调函数，为编程错误，throw掉
        if(!apiFullName||!callback) throw new Error('innerAPI must have apiFullName and callback');

        // 分离服务名和api名
        let [serviceName,apiName]=apiFullName.split('.');

        // 没有服务名时，默认调用自身
        if(!apiName){
            apiName=serviceName;
            serviceName=this.serviceName;
        }

        // 二者缺一不可
        if(!serviceName||!apiName) throw new Error('apiFullName must be [serverName].[apiName]');


        // 如果自己提供该方法，则调用自己，不通过mainControl系统
        if(serviceName===this.serviceName){
            if(apiName in this._innerAPI){
                let realCallback=callback;
                // 回调执行，并清空回调
                this._innerAPI[apiName](json,raw,(...other)=>{
                    realCallback(...other);
                    realCallback=()=>{};
                });
                // 如果有超时设定，则设定定时器
                if(timeout>0){
                    // 到达超时，执行回调并清空
                    setTimeout(()=>{
                        realCallback('innerAPI timeout');
                        realCallback=()=>{};
                    },timeout*1000)
                }
            }
        }
        // 调用eservice系统
        else {
            // 记录回调函数
            const reqNum=this._innerReqNum;
            this._innerReqCallback[reqNum]=callback;

            // 生成请求json
            const reqJson={
                cmd:'innerAPI',
                serviceName:serviceName,
                apiName:apiName,
                reqNum:reqNum,
                json:json
            };

            // 发送请求
            this._econnSocket.send_jr(reqJson,raw,(err)=>{
                // 如果jr消息发送失败
                if(err){
                    // 触发msg事件
                    this._emitMsg('innerAPI req Send Error: '+JSON.stringify(reqJson));

                    // 回调函数库中有
                    if(this._innerReqCallback[reqNum]){
                        // 执行回调，并清除
                        this._innerReqCallback[reqNum]('innerAPI req Send Error: '+JSON.stringify(reqJson));
                        delete this._innerReqCallback[reqNum];
                    }
                }
                if(this._conf.debug)etools.log('[DEBUG] send innerAPI reqNum: '+reqJson.reqNum);
            });


            // 如果有超时设定，则设定定时器
            if(timeout>0){
                setTimeout(()=>{
                    // 检查回调函数库，是否还没有被回调
                    if(this._innerReqCallback[reqNum]){
                        // 还有，真的超时了

                        // 执行错误回调
                        this._innerReqCallback[reqNum]('innerAPI timeout');
                        // 从回调函数库中清除
                        delete this._innerReqCallback[reqNum];
                    }
                },timeout*1000)
            }
        }
    }

    /* 外部API实际上借用内部API途径，指定由httpio服务来处理 */
    // 发布对外API
    regPublicAPI(apiName,func){
        // 输入检验，出错则为编程错误
        if(typeof apiName!=='string'||!apiName) throw new Error('apiName must be STRING');
        if(typeof func!=='function') throw new Error('func must be FUNCTION');

        // 内部记录
        this._publicAPI[apiName]=func;
    }
    // 调用前端API
    wsApi(wsid,apiName,json,raw,callback){
        // 顺序调整
        if(typeof json==='function'){
            callback=json;
            json=raw=undefined;
        } else if(typeof raw==='function'){
            callback=raw;
            raw=undefined;
        }
        // 允许没有callback
        if(!callback) callback=()=>{};

        // 组织json
        const sendJson={
            wsid:wsid,
            apiName:apiName,
            json:json
        };
        // 调用httpio服务内部API
        this.innerAPI('httpio._wsapi',sendJson,raw,callback);
    }
}


/*
* [ mainClient ] 对外事件：
* firstStart            : 自身服务启动
* start                 : 开始监听服务连接
* confInit              : 首次收到配置文件
* confChange            : 收到配置文件变动
* service-on(<obj>)     : 新服务连接
* service-off(<obj>)    : 现有服务退出
* msg :  (<string>)     : 普通消息输出
* warn:  (<ecode>,<msg>): 警告
* stop:  (<ecode>,<err>): 错误，且导致服务停止
* close: (<ecode>,<err>): 监听服务退出
* */