function isObj(obj){
    return !(obj===null
        ||(typeof obj!=='object')
        ||Array.isArray(obj)
        ||Buffer.isBuffer(obj))

}

module.exports={

};