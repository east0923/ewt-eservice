module.exports=function(conf){
    const mainControl=new c_mainControl();
    if(conf) mainControl.setConf(conf);
    return mainControl;
};

const etools=require('ewt-etools');
const econn =require('ewt-econn');

/* ========== 端口监听 ========== */
class c_mainControl extends etools.classBase{
    constructor(conf){
        // 默认配置
        super({
            mainListenPort:null,
            heartTime:20,
            timeOut:10,
            debug:false
        });
        // 管理实例
        this.serviceManage=new c_servicesManage(this);
        // 如果给了conf，则调用setConf
        if(conf){
            this.setConf(conf);
        }
        // 服务配置字典
        this._serviceConfDict={};
    }

    // 实例启动
    _boot(callback){

        // 检查启动环境
        if(this._econnServer){
            callback('_econnServer already exist');
            return
        }
        // 监听实例
        this._econnServer=new econn.tcpServer({
            listenPort:this._conf.mainListenPort,
            ipv4Mask:0,
            fiveCode:0,
            minZipBytes:-1,
            heartTime:this._conf.heartTime,
            timeOut:this._conf.timeOut,
            debug:this._conf.debug
        });
        this._econnServer.on('connection',(serSocket)=>{
            // 生成服务实例
            new c_service_server(serSocket,this.serviceManage)
        });
        this._econnServer.on('error',(code,err)=>{
            callback({
                module:'econn.tcpServer',
                code:code,
                err:err
            });//启动报错
            callback=()=>{};// 清空回调函数
        });
        this._econnServer.on('start',()=>{
            callback();// 启动成功
            callback=()=>{};// 清空回调函数
        });
        this._econnServer.start();
    }

    // 实例停止
    _stop(callback){
        this._econnServer.destroy();
        this._econnServer=null;
        callback();
    }

    /* ========== 对外方法 ========== */
    // 记录配置
    setConf(conf){
        switch (typeof conf){
            case 'number':
            case 'string':
                this._renewConf({mainListenPort:Number.parseInt(conf)});
                break;
            case 'object':
                this._renewConf(conf);
                break;
        }
    }
    // 给某一服务设定配置
    setServiceConf(serviceName,conf){
        // 非对象设置均理解为清除配置
        if(!etools.typeMatch(conf,'object')) conf=undefined;
        // 写字典
        this._serviceConfDict[serviceName]=conf;
        // 向当前所有在线的该服务推送新配置
        this.serviceManage.getServiceByNameAll(serviceName).forEach((service)=>{
            service._serSocket.send_jr({
                cmd:'serviceConf',
                conf:conf
                // 在服务初次登录时，会初次发送当前的配置信息
            })
        })
    }
    // 根据服务名获取配置
    getServiceConf(serviceName){
        // 从字典读取
        const conf=this._serviceConfDict[serviceName];
        // 无配置则反馈空对象，有则克隆一份作为反馈值
        return conf?etools.clone(conf):{}
    }

    /* ========== 对外属性 ========== */
    get listenPort(){
        if(!this._econnServer) return null;
        else return this._econnServer.listenPort;
    }
}

/* ========== 服务管理类及实例 ========== */
class c_servicesManage{
    // 该类是c_mainControl的子类
    constructor(mainControl){
        this.mainControl=mainControl;
        this._s={};     // 服务集合，key为serviceId，value为service实例
        this._dict={};  // 服务字典，key为serverName，value为serviceId数组
        // this._serviceApiCount=0;// 服务间api调用序号计数器
    }

    /* 对外方法 */
    serviceAdd(service){
        // 添加到服务集合
        this._s[service.serviceId]=service;
        // 已有字典项
        if(service.serviceName in this._dict){
            // 字典项中没有该id，添加
            if(!this._dict[service.serviceName].includes(service.serviceId)){
                this._dict[service.serviceName].push(service.serviceId)
            }
            // 字典项中已有该id，什么也不用做
        }
        // 没有字典项，新建
        else {
            this._dict[service.serviceName]=[service.serviceId]
        }
        // 触发外部事件
        this.mainControl.emit('service-on',{
            serviceId:service.serviceId,
            serviceName:service.serviceName
        })
    };    // 添加服务
    serviceRemove(service){
        if(!service) return;
        if(!service.serviceId) return;
        // 从服务集合移除
        delete this._s[service.serviceId];
        // 从字典移除
        if(service.serviceName in this._dict){
            this._dict[service.serviceName].removeElement(service.serviceId);
        }
        this.mainControl.emit('service-off',{
            serviceId:service.serviceId,
            serviceName:service.serviceName
        })
    }; // 移除服务

    // 获取字典
    get serviceDict(){return this._dict}

    // 根据服务名随机获取任一实例
    getServiceByNameRandom(serverName){
        if(serverName in this._dict){
            const arry=this._dict[serverName];
            // 数组长度为0，反馈null
            if(arry.length===0){
                return null
            }
            // 随机获取下标
            const n=Number.parseInt(Math.random()*arry.length);

            // 反馈随机服务实例
            return this._s[arry[n]]
        }
        // 没有获取到，反馈null
        else {
            return null;
        }
    }
    // 根据服务名，获取全部实例
    getServiceByNameAll(serverName){
        // 字典中有该项
        if(serverName in this._dict){
            return Array.from(this._dict[serverName],(serviceId)=>{
                return this._s[serviceId];
            });
        }
        // 没有，反馈空数组
        else {
            return [];
        }
    }
    // 根据sockid获取服务
    getServiceByServerId(serverId){
        // 直接查找在线字典
        if(serverId in this._s){
            return this._s[serverId];
        } else {
            return null;
        }
    }

}
class c_service_server{
    constructor(serSocket,serviceManage){
        this._serSocket=serSocket;
        this._manage=serviceManage;
        this._mainControl=serviceManage.mainControl;
        this._serviceId=etools.sockId(serSocket.host,serSocket.port);

        // 事件
        serSocket.on('error'    ,(...other)=>this._socketOn_error    .apply(this,other));
        serSocket.on('rec_jr'   ,(...other)=>this._socketOn_rec_jr   .apply(this,other));
    }
    _destroy(){
        this._serSocket.destroy();
    }

    /* 连接事件处理 */
    _socketOn_error(a,b){
        // 错误事件向上级报告
        this._mainControl._emitWarn(1,'serSocket Error, serviceName: '+this.serviceName)
        // 从管理集合中移除
        this._manage.serviceRemove(this);
        // 销毁自身
        this._destroy();
    }           // 连接中断即销毁
    _socketOn_rec_jr(json,raw){
        if(typeof this['_jr_'+json.cmd]==='function') this['_jr_'+json.cmd](json,raw)
    }

    /* 收到jr消息处理 */
    _jr_service_client_login(json){
        // 记录服务名
        this.serviceName=json.serviceName;
        // 向管理集合注册
        this._manage.serviceAdd(this);
        // 向client端推送配置
        const conf=this._manage.mainControl.getServiceConf(this.serviceName);
        this._serSocket.send_jr({
            cmd:'serviceConf',
            conf:conf
            // 当mainControl方法setServiceConf调用时，会发送同样的jr消息
        },(err)=>{

        })
    }      // 此消息为服务启动
    _jr_innerAPI(reqJson,raw){
        // 尝试通过管理类，来获取对应服务的实例
        const wannaService=this._manage.getServiceByNameRandom(reqJson.serviceName);

        // 如果没有获取到，反馈错误
        if(!wannaService){
            this._send_innerAPI_Bak(reqJson.reqNum,'cannot find online Service: '+reqJson.serviceName)
        }
        // 获取到，则调用对应实例的中转请求方法
        else{
            wannaService.tranInnerAPI(this,reqJson,raw,(err)=>{
                // 如果发送错误，则回调
                if(err){
                    this._send_innerAPI_Bak(reqJson.reqNum,err)
                }
            })
        }
    }           // 收到client内部API请求
    _jr_tranInnerAPI_Bak(resJson,raw){
        // 通过sockid查找回应的实例
        const toServiceObj=this._manage.getServiceByServerId(resJson.serviceId);
        if(!toServiceObj){
            this._mainControl._emitWarn(0,'rec tranInnerAPI_bak, but no Service to Receive');
            return;
        }

        // 调用对应实例的接收方法
        toServiceObj.tranInnerAPI_Bak(resJson,raw);
    }   // 收到client对中转API请求的反馈
    /* 对外属性 */
    get serviceId(){return this._serviceId}
    /* 对外方法 */
    tranInnerAPI(fromServiceObj,reqJson,raw,callback){
        // 将中转信息写入reqJson
        reqJson.cmd='tranInnerAPI';
        reqJson.serviceId=fromServiceObj.serviceId;

        // 将请求发送到服务实例
        this._serSocket.send_jr(reqJson,raw,(err)=>{
            if(err){
                callback('[tranInnerAPI]'+err);
            } else {
                callback()
            }
        })
    }       // 其它服务发出中转innerAPI调用请求
    tranInnerAPI_Bak(resJson,raw){
        // 将结果发送到客户端
        this._send_innerAPI_Bak(resJson.reqNum,resJson.err,resJson.json);

    }                           // 其它服务反馈innerAPI调用结果
    _send_innerAPI_Bak(reqNum,err,json,raw){
        const resJson={
            cmd:'innerAPI_Bak',
            reqNum:reqNum,
            err:err,
            json:json,
        };
        this._serSocket.send_jr(resJson,undefined,(err)=>{
            if(err){
                this._mainControl._emitWarn(0,'send innerAPI_Bak to client ERROR. '+JSON.stringify(resJson))
            }
        })
    }                 // 向客户端反馈中转innerAPI调用结果
}

/*
* [ mainControl ] 对外事件：
* firstStart            : 初次启动
* start                 : 开始监听服务连接
* service-on(<obj>)     : 新服务连接
* service-off(<obj>)    : 现有服务退出
* msg :  (<string>)     : 普通消息输出
* warn:  (<ecode>,<msg>): 警告
* stop:  (<ecode>,<err>): 错误，且导致服务停止
* close: (<ecode>,<err>): 监听服务退出
* */