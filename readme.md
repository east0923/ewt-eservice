# 功能说明：
## 提供两个类：
### mainControl 服务管理控制类
### mainClient 服务提供端连接库
------
# 各版本更新变化
## v1.0.3
###  修复BUG：
* 1、mainClient与mainControl断开连接时，重连失败不应该再触发offline事件
### 功能添加：
#### 完成外部API调用机制

* 外部API（publicAPI）调用入口统一由httpio服务负责，接收后根据其json字典将publicAPI下发给对应服务。
* 下发过程实际上调用对应服务名为"_publicAPI"的内部api，该内部API已经在mainClient内部提供。
* mainClient实例内部API："_publicAPI"检索服务在mainClient实例中注册的外部API处理方法，调用并反馈。
供端提供集成内部API：“_publicAPI”，供httpio服务来调用外部API。

添加外部API过程：

* 1、编辑httpio服务的json字典文件，将api:[apiName]指定到对应的服务。
* 2、在对应服务中，通过regPublicAPI方法登记[apiName]处理方法，该服务运行后，此API即可对外工作。